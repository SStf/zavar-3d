document.addEventListener("DOMContentLoaded", function(){
    
    var canvas = document.getElementById("mainPlayground");
    var wallWidth = 800;
    var wallHeight = 800;
    
    var engine = new BABYLON.Engine(canvas, true);

    var scene;

    function addBoxMesh(boxWidth, boxHeight){

        var box = BABYLON.MeshBuilder.CreateBox("box", {size: boxWidth, faceColors: BABYLON.Color4(1, 1, 1, 1)}, scene);
        box.collisionsEnabled = true;
        box.checkCollisions = true;
        box.ellipsoid = new BABYLON.Vector3(45, 45, 45);
        box.position.y = boxHeight / 2 | 0 + 20;
        //makes boxes push one another
        box.setPhysicsState(BABYLON.PhysicsEngine.BoxImpostor, { mass: 1, friction: 0.5, restitution: 0.7 });

        return box;
    }

    function addSphereMesh(){
        var sphere = BABYLON.MeshBuilder.CreateSphere("sphere", {diameter:20}, scene);
        sphere.position.y = 20;
        sphere.isPickable = true;
        sphere.checkCollisions = true;
        sphere.ellipsoid = new BABYLON.Vector3(10, 10, 10);
        //makes spheres push one another
        sphere.setPhysicsState(BABYLON.PhysicsEngine.BoxImpostor, { mass: 1, friction: 0.5, restitution: 0.7 });

        return sphere;
    }

//----------------------------------------------Main scene configuration--------------------------------------------------------
    var createScene = function(){
        
        scene = new BABYLON.Scene(engine);
        scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
        scene.clearColor = new BABYLON.Color4(0,0,0,0.0000000000000001);
        scene.collisionsEnabled = true;
        scene.enablePhysics(new BABYLON.Vector3(0, -10, 0), new BABYLON.CannonJSPlugin());

        var camera = new BABYLON.ArcRotateCamera("Camera", 4 * Math.PI, 6 * Math.PI / 24, 15, new BABYLON.Vector3(wallWidth / 2 | 0 + wallWidth / 4 | 0, wallHeight / 2 | 0, 0), scene);
        camera.applyGravity = true;
        camera.attachControl(canvas, true);

        //var light1 = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(100, 100, -100), scene);
        var light0 = new BABYLON.PointLight("Omni0", new BABYLON.Vector3(0, 200, 0), scene);
        light0.intensity = 0.8;

        var sphere = addBoxMesh(100, 100);

        var ground = BABYLON.MeshBuilder.CreateGround("ground", {width:wallWidth, height:wallHeight}, scene);
        ground.isPickable = false;
        
        //adding gravity to ground
        ground.setPhysicsState(BABYLON.PhysicsEngine.BoxImpostor, { mass: 0, friction: 0.5, restitution: 0.1 });
	    ground.updatePhysicsBody();

        var map ={}; //object for multiple key presses
        scene.actionManager = new BABYLON.ActionManager(scene);

//-----------------------------------------------Adding keyboard commands--------------------------------------------------------
        scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, function (evt) {                                
            map[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";

        }));

        scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, function (evt) {                                
            map[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
        }));

        scene.registerAfterRender(function(){
            //Adding movement here after initial positioning of an element
            if ((map["s"] || map["S"]) && sphere.position.y > 1) sphere.translate(BABYLON.Axis.Y, -1.0, BABYLON.Space.LOCAL);
            if (map["w"] || map["W"]) sphere.translate(BABYLON.Axis.Y, 1.0, BABYLON.Space.LOCAL);
            if(map["b"] || map["B"]) {
                addBoxMesh();
            }
        });
//----------------------------------------------End Adding keyboard commands-----------------------------------------------------

//----------------------------------------------Making all objects draggable-----------------------------------------------------
        var getGroundPosition = function () {
            // Use a predicate to get position on the ground
            var pickinfo = scene.pick(scene.pointerX, scene.pointerY, function (mesh) { return mesh == ground; });
            if (pickinfo.hit) {
                return pickinfo.pickedPoint;
            }

            return null;
        };

        var startingPoint;
        var currentMesh;
        
        var onPointerDown = function (event){
            if (event.button !== 0) {
                return;
            }
            var pickInfo = scene.pick(scene.pointerX, scene.pointerY, function (mesh) { return mesh !== ground; });
            if (pickInfo.hit) {
                currentMesh = pickInfo.pickedMesh;
                startingPoint = getGroundPosition(event);

                if (startingPoint) { // we need to disconnect camera from canvas
                    setTimeout(function () {
                        camera.detachControl(canvas);
                    }, 0);
                }
            }
        };

        var onPointerUp = function () {
            if (startingPoint) {
                camera.attachControl(canvas, true);
                startingPoint = null;
                return;
            }
        };

        var onPointerMove = function (event) {
            if (!startingPoint) {
                return;
            }

            var current = getGroundPosition(event);

            if (!current) {
                return;
            }

            var diff = current.subtract(startingPoint);
            currentMesh.moveWithCollisions(diff);
            //currentMesh.position.addInPlace(diff);

            startingPoint = current;

        }

        canvas.addEventListener("pointerdown", onPointerDown, false);
        canvas.addEventListener("pointerup", onPointerUp, false);
        canvas.addEventListener("pointermove", onPointerMove, false);

        scene.onDispose = function () {
            canvas.removeEventListener("pointerdown", onPointerDown);
            canvas.removeEventListener("pointerup", onPointerUp);
            canvas.removeEventListener("pointermove", onPointerMove);
        }
//---------------------------------------------End Making all objects draggable--------------------------------------------------

        return scene;

    };
//-----------------------------------------------End Main scene configuration----------------------------------------------------

    var scene = createScene();

    engine.runRenderLoop(function(){
        scene.render();
    });

    window.addEventListener("resize", function(){
        engine.resize();
    });

    var allAddBoxControls = document.getElementsByClassName("addBox");
    var allAddSphereControls = document.getElementsByClassName("addSphere");

    for(var i = 0; i < allAddBoxControls.length; i++){
        (function(){
            allAddBoxControls[i].addEventListener("click", function(event){
                addBoxMesh(100, 100);
            });
        })();
    }

    for(var i = 0; i < allAddSphereControls.length; i++){
        allAddSphereControls[i].addEventListener("click", addSphereMesh);
    }

});